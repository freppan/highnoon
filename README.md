# HighNoon

Adventure game: It’s high noon somewhere


# Building

## Linux

```
mkdir build && cd build
cmake ..
make
```

## Windows (mingw64)

```
# Setup the environment
Install mingw64 according to their instructions:
https://www.msys2.org/

mkdir build
cd build
cmake .. -G "MinGW Makefiles"
mingw32-make
cd ..

```

## gdb with cmake
add -DCMAKE_BUILD_TYPE=Debug to the cmake command of your system.
build like usual
gdb highnoon (.exe for windows)

gdb in 60 seconds with our lord and savior Jacob Sorber:
https://www.youtube.com/watch?v=mfmXcbiRs0E

```