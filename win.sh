rm -rf build
mkdir build
cd build
export CC=/usr/bin/x86_64-w64-mingw32-gcc
export CXX=/usr/bin/x86_64-w64-mingw32-g++
cmake ..
make
cd ..
