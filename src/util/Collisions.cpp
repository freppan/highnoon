#include "Collisions.hpp"
#include <iostream>

bool Collisions::CheckCollision(Entity &ent0, Entity &ent1)
{
    std::string hitbox0 = ent0.GetHitbox()->GetDescription();
    std::string hitbox1 = ent1.GetHitbox()->GetDescription();

    if (hitbox0 == "NoHitbox" || hitbox1 == "NoHitbox")
    {
        return false;
    }
    else if (hitbox0 == "CircularHitbox" && hitbox1 == "CircularHitbox")
    {
        return CheckCollisionCircularToCircular(ent0, ent1);
    }
    else if (hitbox0 == "CircularHitbox" && hitbox1 == "RectangularHitbox")
    {
        return CheckCollisionCircularToRectangular(ent0, ent1);
    }
    else if (hitbox0 == "RectangularHitbox" && hitbox1 == "CircularHitbox")
    {
        return CheckCollisionRectangularToCircular(ent0, ent1);
    }
    else if (hitbox0 == "RectangularHitbox" && hitbox1 == "RectangularHitbox")
    {
        return CheckCollisionRectangularToRectangular(ent0, ent1);
    }
    else
    {
        std::cout << "No Collision Pattern Detected!\n";
        return false;
    }
}

bool Collisions::CheckCollisionCircularToCircular(Entity &ent0, Entity &ent1)
{
    Pos pos0 = ent0.GetPos();
    Pos pos1 = ent1.GetPos();

    pos0 = ent0.GetHitbox()->ApplyOffset(pos0);
    pos1 = ent1.GetHitbox()->ApplyOffset(pos1);

    float radius0 = ent0.GetHitbox()->GetRadius();
    float radius1 = ent1.GetHitbox()->GetRadius();

    // Basic distance check to abool heavy operations on every collision check
    if (!(abs(pos0.x - pos1.x) < radius0 + radius1))
    {
        return false;
    }
    if (!(abs(pos0.y - pos1.y) < radius0 + radius1))
    {
        return false;
    }

    // Collision Check
    if ((pos0.x - pos1.x) * (pos0.x - pos1.x) + (pos0.y - pos1.y) * (pos0.y - pos1.y) < (radius0 + radius1) * (radius0 + radius1)) // Don't sqrt when we dont need to. Just like in real life.
    {
        float distance = sqrtf((pos0.x - pos1.x) * (pos0.x - pos1.x) + (pos0.y - pos1.y) * (pos0.y - pos1.y));
        float overlap = distance - radius0 - radius1;
        pos0.x -= overlap * (pos0.x - pos1.x) / distance;
        pos0.y -= overlap * (pos0.y - pos1.y) / distance;
        ent0.SetPos(pos0);
        return true;
    }
    else
    {
        return false;
    }
}

bool Collisions::CheckCollisionCircularToRectangular(Entity &ent0, Entity &ent1)
{
    Pos pos0 = ent0.GetPos();
    Pos pos1 = ent1.GetPos();

    pos0 = ent0.GetHitbox()->ApplyOffset(pos0);
    pos1 = ent1.GetHitbox()->ApplyOffset(pos1);

    float radius = ent0.GetHitbox()->GetRadius();
    Pos dimension1 = ent1.GetHitbox()->GetDimensions();

    float rectLeftEdge = pos1.x - dimension1.x / 2;
    float rectRightEdge = pos1.x + dimension1.x / 2;
    float rectTopEdge = pos1.y - dimension1.y / 2;
    float rectBotEdge = pos1.y + dimension1.y / 2;

    float xDistToRect = std::max(rectLeftEdge, std::min(rectRightEdge, pos0.x)) - pos0.x;
    float yDistToRect = std::max(rectTopEdge, std::min(rectBotEdge, pos0.y)) - pos0.y;

    if (xDistToRect * xDistToRect + yDistToRect * yDistToRect < radius * radius)
    {
        float distToRect = sqrtf(xDistToRect * xDistToRect + yDistToRect * yDistToRect);
        float overlap = radius - distToRect;

        pos0.x -= xDistToRect * overlap / distToRect;
        pos0.y -= yDistToRect * overlap / distToRect;

        ent0.SetPos(pos0);
        return true;
    }
    else
    {
        return false;
    }
}

bool Collisions::CheckCollisionRectangularToCircular(Entity &ent0, Entity &ent1)
{ // Implement if needed
  // Reverse engineer Collisions::CheckCollisionCircularToRectangular should be easy enough
    /*
        std::pair<float, float> pos0 = ent0.GetPos();
        std::pair<float, float> pos1 = ent1.GetPos();

        pos0 = ent0.GetHitbox()->ApplyOffset(pos0);
        pos1 = ent1.GetHitbox()->ApplyOffset(pos1);

        fDimension dimension0 = ent0.GetHitbox()->GetDimensions();
        float radius1 = ent1.GetHitbox()->GetRadius();
    */
   return false;
}

bool Collisions::CheckCollisionRectangularToRectangular(Entity &ent0, Entity &ent1)
{ // Implement if needed
  // https://youtu.be/8JJ-4JgR7Dg
    /*

        std::pair<float, float> pos0 = ent0.GetPos();
        std::pair<float, float> pos1 = ent1.GetPos();

        pos0 = ent0.GetHitbox()->ApplyOffset(pos0);
        pos1 = ent1.GetHitbox()->ApplyOffset(pos1);

        Pos dim0 = ent0.GetHitbox()->GetDimensions();
        Pos dim1 = ent1.GetHitbox()->GetDimensions();
    */
   return false;
}