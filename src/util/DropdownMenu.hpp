#ifndef DROPDOWN_MENU_H
#define DROPDOWN_MENU_H

#include <string>
#include <vector>
#include "PixelPos.hpp"
#include "GraphicsManager.hpp"
#include "Constants.hpp"


class Element;
class DropdownMenu
{
private:
    std::vector<Element> m_elements = {};
    bool m_expanded = false;
    Pixel m_dimension; // Menu dimensions
    Pixel m_mousePixel;

public:
    DropdownMenu(std::vector<Element> elements);
    ~DropdownMenu();

    bool Expanded();
    void SetMousePixel(Pixel pixel);
    void CalcRenderPixel(Pixel pixel);
    void Render();

    std::pair<bool, int> Click(Pixel pixel);
    void Toggle(Pixel pixel);
    void Expand(Pixel pixel);
    void Close();
};

class Element
{
private:
    std::string m_name;
    Pixel m_renderPixel; // Top left pixel of element
    Pixel m_dimension;
    Texture *m_label;
    DropdownMenu m_dropdownMenu;
    bool m_hasDropdownMenu;
    int m_activationId;

public:
    Element(Pixel dimension, std::string label, int activationId, std::vector<Element> elements = {});
    ~Element();

    bool IsWithinBorders(Pixel pixel);
    Pixel GetDimension();
    void SetWidth(int width);
    void SetRenderPixel(Pixel pixel);
    Pixel GetRenderPixel();
    Texture *GetLabel();
    void Close();
    std::pair<bool, int> Click(Pixel pixel);
    void Render(Pixel mousePixel);
};

#endif