#include "DropdownMenu.hpp"
#include <iostream>

DropdownMenu::DropdownMenu(std::vector<Element> elements)
    : m_elements(elements)
{
    m_dimension = {0, 0};
    if (m_elements.empty())
    {
        return;
    }

    for (auto &element : m_elements)
    {
        m_dimension.x = std::max(m_dimension.x, element.GetDimension().x);
        m_dimension.y += element.GetDimension().y;
    }

    for (auto &element : m_elements)
    {
        element.SetWidth(m_dimension.x);
    }
}

DropdownMenu::~DropdownMenu()
{
}

bool DropdownMenu::Expanded()
{
    return m_expanded;
}

void DropdownMenu::SetMousePixel(Pixel pixel)
{
    m_mousePixel = pixel;
}

void DropdownMenu::CalcRenderPixel(Pixel pixel)
{
    // Dont render outside bot or right edge
    pixel.x = std::min(pixel.x, int(SCREENWIDTH) - m_dimension.x);
    pixel.y = std::min(pixel.y, int(SCREENHEIGHT) - m_dimension.y);

    // Left and top has prio over bot or right if menu doesn't fit on screen
    pixel.x = std::max(pixel.x, 0);
    pixel.y = std::max(pixel.y, 0);

    int yAccumulate = pixel.y;
    for (auto &element : m_elements)
    {
        element.SetRenderPixel({pixel.x, yAccumulate});
        yAccumulate += element.GetDimension().y;
    }
}

void DropdownMenu::Render()
{
    if (!m_expanded)
    {
        return;
    }

    for (auto &element : m_elements)
    {
        element.Render(m_mousePixel);
    }
}

std::pair<bool, int> DropdownMenu::Click(Pixel pixel)
{
    if (!m_expanded)
    {
        return {false, 0};
    }

    bool clicked = 0;
    int value = 0;
    for (auto &element : m_elements)
    {
        std::pair<bool, int> res = element.Click(pixel);
        clicked |= res.first;
        value += res.second;
    }

    if (!clicked)
    {
        Close();
    }
    return {clicked, value};
}

void DropdownMenu::Toggle(Pixel pixel)
{
    if (m_expanded)
    {
        Close();
    }
    else
    {
        CalcRenderPixel(pixel);
        m_expanded = true;
    }
}

void DropdownMenu::Expand(Pixel pixel)
{
    CalcRenderPixel(pixel);
    m_expanded = true;
}

void DropdownMenu::Close()
{
    m_expanded = false;
    for (auto &element : m_elements)
    {
        element.Close();
    }
}

/* ------------- */
/*               */
/*    Element    */
/*               */
/* ------------- */

Element::Element(Pixel dimension, std::string label, int id, std::vector<Element> elements)
    : m_name(label),
      m_dimension(dimension),
      m_label(g_gfx.RequestTexture(label, 20, {255, 255, 255}, "assets/fonts/TEMPSITC.TTF")),
      m_dropdownMenu(DropdownMenu(elements)),
      m_activationId(id)
{
    m_hasDropdownMenu = (elements.size() > 0);
}

Element::~Element()
{
}

bool Element::IsWithinBorders(Pixel pixel)
{
    return (pixel.x > m_renderPixel.x && pixel.y > m_renderPixel.y && pixel.x < m_renderPixel.x + m_dimension.x && pixel.y < m_renderPixel.y + m_dimension.y);
}

Pixel Element::GetDimension()
{
    return m_dimension;
}

void Element::SetWidth(int width)
{
    m_dimension.x = width;
}

Pixel Element::GetRenderPixel()
{
    return m_renderPixel;
}

void Element::SetRenderPixel(Pixel pixel)
{
    m_renderPixel = pixel;
}

Texture *Element::GetLabel()
{
    return m_label;
}

void Element::Close()
{
    if (m_hasDropdownMenu)
    {
        m_dropdownMenu.Close();
    }
}

std::pair<bool, int> Element::Click(Pixel pixel)
{
    if (IsWithinBorders(pixel))
    {
        if (m_hasDropdownMenu)
        {
            m_dropdownMenu.Toggle({m_renderPixel.x + m_dimension.x, m_renderPixel.y});
            return {true, m_activationId};
        }       
        return {false, m_activationId};
    }
    return m_dropdownMenu.Click(pixel);
}

void Element::Render(Pixel mousePixel)
{
    if (IsWithinBorders(mousePixel))
    {
        g_gfx.SetDrawColor(120, 120, 200, 0);
    }
    else
    {
        g_gfx.SetDrawColor(100, 100, 100, 0);
    }
    g_gfx.FillRectangle(m_renderPixel, m_dimension);
    g_gfx.SetDrawColor(0, 0, 0, 0);
    g_gfx.DrawRectangle(m_renderPixel, m_dimension);
    g_gfx.RenderRaw(*m_label, m_renderPixel);

    if (m_hasDropdownMenu)
    {
        m_dropdownMenu.Render();
    }
}