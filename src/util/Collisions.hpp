#ifndef COLLISIONS_H
#define COLLISIONS_H

#include "Hitboxes.hpp"
#include "entities/Entity.hpp"

class Collisions
{
public:
	static bool CheckCollision(Entity &ent0, Entity &ent1);
	static bool CheckCollisionCircularToCircular(Entity &ent0, Entity &ent1);
	static bool CheckCollisionCircularToRectangular(Entity &ent0, Entity &ent1);
	static bool CheckCollisionRectangularToCircular(Entity &ent0, Entity &ent1);
	static bool CheckCollisionRectangularToRectangular(Entity &ent0, Entity &ent1);
};

#endif