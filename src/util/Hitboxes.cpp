#include "Hitboxes.hpp"

Hitbox::Hitbox(Pixel offset)
    : m_offset(offset)
{
}

Pos Hitbox::ApplyOffset(Pos pos)
{
    return {pos.x + float(m_offset.x) / float(TILESIZE), pos.y + float(m_offset.y) / float(TILESIZE)};
}

Pixel Hitbox::ApplyOffset(Pixel pixel)
{
    return {pixel.x + int(m_offset.x * SCALE), pixel.y + int(m_offset.y * SCALE)};
}

NoHitbox::NoHitbox(Pixel offset)
    : Hitbox(offset)
{
    m_description = "NoHitbox";
}

CircularHitbox::CircularHitbox()
    : Hitbox({0, 0}),
      m_radius(0)
{
    m_description = "CircularHitbox";
}

CircularHitbox::CircularHitbox(int radius, Pixel offset)
    : Hitbox(offset),
      m_radius(float(radius) / TILESIZE)
{
    m_description = "CircularHitbox";
}

float CircularHitbox::GetRadius()
{
    return m_radius;
}

RectangularHitbox::RectangularHitbox()
    : Hitbox({0, 0}),
      m_width(0),
      m_height(0)
{
    m_description = "RectangularHitbox";
}

RectangularHitbox::RectangularHitbox(Pixel dimension, Pixel offset)
    : Hitbox(offset),
      m_width(float(dimension.x) / TILESIZE),
      m_height(float(dimension.y) / TILESIZE)
{
    m_description = "RectangularHitbox";
}

Pos RectangularHitbox::GetDimensions()
{
    return {m_width, m_height};
}

Pixel RectangularHitbox::GetPixelDimensions()
{
    return {int(m_width * TILESIZE), int(m_height * TILESIZE)};
}