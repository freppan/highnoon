#ifndef HITBOXES_H
#define HITBOXES_H
#include <stdlib.h>
#include <string>
#include "Constants.hpp"
#include "GraphicsManager.hpp"
#include "util/PixelPos.hpp"

class Hitbox
{
protected:
    std::string m_description = "BaseHitbox";
    Pixel m_offset;

public:
    Hitbox(Pixel offset = {0, 0});
    std::string GetDescription() { return m_description; };
    virtual float GetRadius() { return 0.0f; };
    virtual Pos GetDimensions() { return {0.0f, 0.0f}; };
    virtual Pixel GetPixelDimensions() { return {0, 0}; };
    Pixel GetOffset() { return m_offset; }
    Pos ApplyOffset(Pos pos);
    Pixel ApplyOffset(Pixel pixel);
};

class NoHitbox : public Hitbox
{
public:
    NoHitbox(Pixel offset = {0, 0});
};

class CircularHitbox : public Hitbox
{
private:
    float m_radius; // Input nr of pixels
public:
    CircularHitbox();
    CircularHitbox(int radius, Pixel offset = {0, 0});
    float GetRadius() override;
};

class RectangularHitbox : public Hitbox
{
private:
    float m_width; // Input nr of pixels
    float m_height;

public:
    RectangularHitbox();
    RectangularHitbox(Pixel dimension, Pixel offset = {0, 0});
    Pos GetDimensions() override;
    Pixel GetPixelDimensions() override;
};

#endif