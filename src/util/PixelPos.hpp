#ifndef PixelPos_H
#define PixelPos_H

#include <math.h>
#include <iostream>

template <class T>
struct vect
{
    T x = 0;
    T y = 0;
    vect() : x(0), y(0) {}
    vect(T _x, T _y) : x(_x), y(_y) {}
    vect floor() const { return vect(std::floor(x), std::floor(y)); }
    bool operator==(const vect &rhs) const { return (this->x == rhs.x && this->y == rhs.y); }
    bool operator!=(const vect &rhs) const { return (this->x != rhs.x || this->y != rhs.y); }
    vect operator+(const vect &rhs) const { return vect(this->x + rhs.x, this->y + rhs.y); }
    vect operator-(const vect &rhs) const { return vect(this->x - rhs.x, this->y - rhs.y); }
    vect operator*(const T &rhs) const { return vect(T(this->x * rhs), T(this->y * rhs)); } // Non-commutative implementation so far
    vect operator/(const T &rhs) const { return vect(T(this->x / rhs), T(this->y / rhs)); } // Non-commutative implementation so far
    T magnitude() const { return T(std::sqrt(x * x + y * y)); }
    friend std::ostream &operator<<(std::ostream &os, const vect &rhs)
    {
        os << "{ x: " + std::to_string(rhs.x) + ", y: " + std::to_string(rhs.y) + " }\n";
        return os;
    }
};

typedef vect<int> Pixel;
typedef vect<float> Pos;

#endif