//Using SDL, SDL_image, SDL_mixer, standard IO, and strings
// Mix_Chunk, a sound clip (In channels).
// Mix_Music, a longer sound clip to be used as background music (Not in channels).
#include "SoundManager.hpp"

    SoundManager::SoundManager()
    {
        //Initialize SDL_mixer
        if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 )
        {
            printf( "SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError() );
        }
    }


    SoundManager::~SoundManager()
    {
        FreeAll();
        Mix_Quit();
    }


    bool SoundManager::LoadMusic(std::string name, const char* path)
    {
        Mix_Music* music = Mix_LoadMUS(path);
        if( music == NULL )
        {
            printf( "Failed to load high sound effect! SDL_mixer Error: %s\n", Mix_GetError() );
            return false;
        }
        m_music_map.insert( std::pair<std::string, Mix_Music*>(name, music) );
        return true;
    }


    bool SoundManager::LoadSfx(std::string name, const char* path)
    {
        Mix_Chunk* sfx = Mix_LoadWAV(path);
        if( sfx == NULL )        
        {
            printf( "Failed to load high sound effect! SDL_mixer Error: %s\n", Mix_GetError() );
            return false;
        }
        m_sfx_map.insert( std::pair<std::string, Mix_Chunk*>(name, sfx) );
        return true;
    } 


    void SoundManager::FreeMusic(std::string name)
    { 
        Mix_FreeMusic( m_music_map.at(name) );
        m_music_map.erase(name);
    }


    void SoundManager::FreeSfx(std::string name)
    { 
        Mix_FreeChunk( m_sfx_map.at(name) );
        m_sfx_map.erase(name);
    }


    void SoundManager::FreeAllMusic()
    { 
        for (auto& it: m_music_map) {
            Mix_FreeMusic(it.second);
        }
        m_music_map.clear();
    }


    void SoundManager::FreeAllSfx()
    { 
        for (auto& it: m_sfx_map) {
            Mix_FreeChunk(it.second);
        }
        m_sfx_map.clear();
    }


    void SoundManager::FreeAll()
    {
        FreeAllMusic();
        FreeAllSfx();
    }


    void SoundManager::PlayMusic(std::string name, int loops)
    {
        //loops = -1 plays forever (or as close at it can to forever)
        Mix_PlayMusic( m_music_map.at(name), loops);
    }


    void SoundManager::StopMusic()
    {
        //loops = -1 plays forever (or as close at it can to forever)
        Mix_HaltMusic();
    }


    void SoundManager::PlaySfx(std::string name, int loops)
    {
        Mix_PlayChannel( -1, m_sfx_map.at(name), loops - 1 );
    }

    int SoundManager::SetMusicVol(int Volume)
    {
        return (Mix_VolumeMusic(Volume));
        //printf("volume is now : %d\n", Mix_VolumeMusic(-1));    
    }

    SoundManager g_sfx;