#ifndef GRAPHICS_MANAGER_H
#define GRAPHICS_MANAGER_H

#include <cstdio>
#include <string>
#include <unordered_map>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include "util/PixelPos.hpp"

class GraphicsManager;

class Texture {
private:
    SDL_Texture* m_texture;
    Pixel m_size;
    Pixel m_spriteDimensions;
    bool m_staticSprite;
    friend class GraphicsManager;

    SDL_Texture* getTexture();
public:
    Texture(SDL_Renderer* renderer, std::string texturePath, int spriteRows, int spriteCols);
    Texture(SDL_Renderer* renderer, std::string text, SDL_Color color, int fontSize, std::string fontPath);
    void Scale(float scale);
    int width()
    {
        return m_size.x;
    }
    int height()
    {
        return m_size.y;
    }
    int spriteRows()
    {
        return m_spriteDimensions.x;
    }
    int spriteCols()
    {
        return m_spriteDimensions.y;
    }
    bool staticSprite()
    {
        return m_staticSprite;
    }
    void ReleaseTexture();
    void ColorMod(Uint8 r, Uint8 g, Uint8 b);
};

class GraphicsManager {
private:
    SDL_Window* m_window;
    SDL_Renderer* m_renderer;
    std::unordered_map<std::string, Texture> m_textures;
    SDL_Rect GetRect(Texture& texture, Pixel& destPixel);

public:
    GraphicsManager();
    ~GraphicsManager();
    void ResetRenderArea();
    void Render(SDL_Texture* texture, SDL_Rect& animationFrame, SDL_Rect& dstrect);
    void Render(SDL_Texture* texture, SDL_Rect& dstrect);
    void Render(Texture& texture, Pixel& destPixel);
    void Render(Texture& texture, SDL_Rect& animationFrame, Pixel& destPixel);
    void RenderRaw(Texture& texture, Pixel& destPixel);
    void RenderBuffer();
    Texture* RequestTexture(std::string texturePath, int spriteRows = 1, int spriteCols = 1);
    Texture* RequestTexture(std::string text, int fontSize, SDL_Color color, std::string fontPath = "assets/fonts/Serpentine-yw3n3.ttf");
    SDL_Renderer* GetRenderer();
    void UnloadTextures();

    //Spriteless Pixel Manipulation
    void SetDrawColor(int red, int green, int blue, int alpha = 0);
    void DrawRectangle(Pixel pixel, Pixel dimension);
    void FillRectangle(Pixel pixel, Pixel dimension);

};

extern GraphicsManager g_gfx;
#endif //GRAPHICS_MANAGER_H