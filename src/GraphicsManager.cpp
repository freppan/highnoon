#include "GraphicsManager.hpp"
#include "Constants.hpp"
#include <cstdio>
#include <iostream>

GraphicsManager::GraphicsManager()
{
    if (SDL_Init(SDL_INIT_TIMER | SDL_INIT_VIDEO) != 0) 
    {
        printf("error initializing SDL: %s\n", SDL_GetError());
    }
    m_window = SDL_CreateWindow("GAME",
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        SCREENWIDTH, SCREENHEIGHT, 0);

    Uint32 render_flags = SDL_RENDERER_ACCELERATED; //GPU Acceleration enabled
    m_renderer = SDL_CreateRenderer(m_window, -1, render_flags);
	TTF_Init();
}

GraphicsManager::~GraphicsManager()
{
	TTF_Quit();
    for (auto& it : m_textures)
    {
        it.second.ReleaseTexture();
    }
    SDL_DestroyRenderer(m_renderer);
    SDL_DestroyWindow(m_window);
    SDL_Quit();
}

void GraphicsManager::ResetRenderArea()
{
    SDL_SetRenderDrawColor(m_renderer, 0, 0, 0, 0);
    SDL_RenderClear(m_renderer);
}

SDL_Rect GraphicsManager::GetRect(Texture& texture, Pixel& destPixel)
{
    return {
        int(destPixel.x - SCALE * (texture.width() / texture.spriteCols()) / 2),
        int(destPixel.y - SCALE * (texture.height() / texture.spriteRows()) / 2),
        texture.width() * SCALE / texture.spriteCols(),
        texture.height()  * SCALE / texture.spriteRows()};
}

void GraphicsManager::Render(SDL_Texture* texture, SDL_Rect& animationFrame, SDL_Rect& dstrect)
{
    int status = SDL_RenderCopy(m_renderer, texture, &animationFrame, &dstrect);
}

void GraphicsManager::Render(SDL_Texture* texture, SDL_Rect& dstrect)
{
    int status = SDL_RenderCopy(m_renderer, texture, NULL, &dstrect);
}

void GraphicsManager::Render(Texture& texture, Pixel& destPixel)
{
    SDL_Rect dstrect = GetRect(texture, destPixel);
    SDL_Texture* textureCtx = texture.getTexture();
    int status = SDL_RenderCopy(m_renderer, textureCtx, NULL, &dstrect);
}

void GraphicsManager::Render(Texture& texture, SDL_Rect& animationFrame, Pixel& destPixel)
{
    SDL_Rect dstrect = GetRect(texture, destPixel);
    SDL_Texture* textureCtx = texture.getTexture();
    int status = SDL_RenderCopy(m_renderer, textureCtx, &animationFrame, &dstrect);
}

void GraphicsManager::RenderRaw(Texture& texture, Pixel& destPixel)
{

    SDL_Rect dstrect = 
    {
        destPixel.x,
        destPixel.y,
        texture.width(),
        texture.height()
    };
    SDL_Texture* textureCtx = texture.getTexture();
    int status = SDL_RenderCopy(m_renderer, textureCtx, NULL, &dstrect);
}

void GraphicsManager::RenderBuffer()
{
    SDL_RenderPresent(m_renderer);
}

void GraphicsManager::UnloadTextures()
{
    for (auto& it : m_textures)
    {
        it.second.ReleaseTexture();
    }
    m_textures.clear();
}

SDL_Renderer* GraphicsManager::GetRenderer()
{
    return m_renderer;
}

Texture* GraphicsManager::RequestTexture(std::string texturePath, int spriteRows, int spriteCols)
{
    if (m_textures.count(texturePath) == 0)
    {
        Texture requestedTexture = Texture(m_renderer, texturePath, spriteRows, spriteCols);
        m_textures.insert({texturePath, requestedTexture});
    }
    return &m_textures.at(texturePath);
}

Texture* GraphicsManager::RequestTexture(std::string text, int fontSize, SDL_Color color, std::string fontPath)
{
    std::string textureName = text + std::to_string(fontSize);
    if (m_textures.count(textureName) == 0)
    {
        Texture requestedTexture = Texture(m_renderer, text, color, fontSize, fontPath);
        m_textures.insert({textureName, requestedTexture});
    }

    return &m_textures.at(textureName);
}

void GraphicsManager::SetDrawColor(int red, int green, int blue, int alpha)
{
    SDL_SetRenderDrawColor(m_renderer, red, green, blue, alpha);
}

void GraphicsManager::DrawRectangle(Pixel pixel, Pixel dimension)
{
    SDL_Rect dstrect = 
    {
        pixel.x,
        pixel.y,
        dimension.x,
        dimension.y
    };
    SDL_RenderDrawRect(m_renderer, &dstrect);
}

void GraphicsManager::FillRectangle(Pixel pixel, Pixel dimension)
{
    SDL_Rect dstrect = 
    {
        pixel.x,
        pixel.y,
        dimension.x,
        dimension.y
    };
    SDL_RenderFillRect(m_renderer, &dstrect);
}

Texture::Texture(SDL_Renderer* renderer, std::string texturePath, int spriteRows ,int spriteCols)
{
    SDL_Surface* tempSurface = IMG_Load(texturePath.c_str());
    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, tempSurface);
    SDL_FreeSurface(tempSurface);
    int width, height;
    SDL_QueryTexture(texture, NULL, NULL, &width, &height);
    m_size = {width, height};
    m_spriteDimensions = {spriteRows, spriteCols};
    m_staticSprite = (spriteRows == 1 && spriteCols == 1);
    m_texture = texture;
}

Texture::Texture(SDL_Renderer* renderer, std::string text, SDL_Color color, int fontSize, std::string fontPath)
{
    TTF_Font* font = TTF_OpenFont(fontPath.c_str(), fontSize);
    SDL_Surface* tempSurface = TTF_RenderText_Solid(font, text.c_str(), color);
    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, tempSurface);
    SDL_FreeSurface(tempSurface);
    SDL_QueryTexture(texture, NULL, NULL, &m_size.x, &m_size.y);

    m_spriteDimensions = {1, 1};
    m_staticSprite = true;
    m_texture = texture;
    TTF_CloseFont(font);
}

void Texture::Scale(float scale)
{
    m_size.x *= scale;
    m_size.y *= scale;
}

SDL_Texture* Texture::getTexture()
{
    return m_texture;
}

void Texture::ReleaseTexture()
{
    SDL_DestroyTexture(m_texture);
}

void Texture::ColorMod(Uint8 r, Uint8 g, Uint8 b)
{
    SDL_SetTextureColorMod(m_texture, r, g, b);
}

GraphicsManager g_gfx;