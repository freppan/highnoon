//Using SDL, SDL_image, SDL_mixer, standard IO, and strings

#ifndef SOUND_MANAGER_H
#define SOUND_MANAGER_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <stdio.h>
#include <string>
#include <unordered_map>

class SoundManager{
private:

    std::unordered_map<std::string, Mix_Chunk*> m_sfx_map;
    std::unordered_map<std::string, Mix_Music*> m_music_map;

public:

    SoundManager();
    ~SoundManager();

    bool LoadMusic(std::string, const char* path);
    bool LoadSfx(std::string, const char* path);

    void FreeMusic(std::string);
    void FreeSfx(std::string);
    void FreeAllMusic();
    void FreeAllSfx();
    void FreeAll();

    void PlayMusic(std::string, int loops = -1); //loops = -1 plays forever (or as close at it can to forever)
    void PlaySfx(std::string, int loops = 1);
    void StopMusic();
    int SetMusicVol(int Volume);
};

extern SoundManager g_sfx;
#endif //SOUND_MANAGER_H