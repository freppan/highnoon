#ifndef Entity_H
#define Entity_H

#include <math.h>
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>
#include "Constants.hpp"
#include "SoundManager.hpp"
#include "GraphicsManager.hpp"
#include "util/Hitboxes.hpp"
#include "util/PixelPos.hpp"

class Entity
{
protected:
	std::string m_uid;
	Texture *m_texture;
	SDL_Rect m_animationFrame; // specific frame on a texture (to create animation).

	float m_speed = 8.0f;
	Pixel m_direction = {0, 0};
	int m_xMax, m_yMax;
	float m_countTime = 0;
	Pos m_pos;
	Pixel m_spriteOffset = {0, 0};
	struct Color
	{
		Uint8 r = 255;
		Uint8 g = 255;
		Uint8 b = 255;
		bool operator==(const Color &rhs) const { return (this->r == rhs.r && this->g == rhs.g && this->b == rhs.b); }
		bool operator!=(const Color &rhs) const { return (this->r != rhs.r || this->g != rhs.g || this->b != rhs.b); }
	} m_colorMod;

	//Damage()
	//float m_health = 100.0f;
	float m_resetColorModTimer = 0.0f;

	//Fling()
	Pos m_flingDirection = {0.0f, 0.0f};
	float m_flingSpeed = 0.0f;

public:
	NoHitbox m_noHitbox;
	RectangularHitbox m_rectangularHitbox;
	CircularHitbox m_circularHitbox;
	Hitbox *m_hitbox;

	enum Direction
	{
		DIR_RIGHT,
		DIR_DOWN,
		DIR_LEFT,
		DIR_UP
	};

	void SetSpriteDirection(Entity::Direction);
	void SetPos(Pos pos);
	Pos GetPos();
	void Damage(float damage, Color color = {255, 100, 100});
	void Fling(Pos direction, float speed);
	void HandleFling(double elapsedTime);
	std::string GetUid();
	void SetColorMod(Uint8 r, Uint8 g, Uint8 b);
	void SetMapBorders(int x, int y);
	void RenderPos(Pixel pixel);
	Hitbox *GetHitbox();
	void RenderHitbox(Pixel pixel);			   // For debug purposes
	void RenderCircularHitbox(Pixel pixel);	   // For debug purposes
	void RenderRectangularHitbox(Pixel pixel); // For debug purposes
	void RenderGraphics(Pixel pixel, double elapsedTime);
	void Update(double elapsedTime);
	virtual void ChildUpdate(double elapsedTime);
	virtual Entity *Clone();

	Entity(
		std::string uid = "",
		Pos pos = {0, 0},
		std::string sprite = std::string("assets/sprites/misc/backupSprite.png"),
		Pixel hitboxDimension = {0, 0},
		Pixel hitboxOffset = {0, 0},
		int spriteRows = 1,
		int spriteCols = 1,
		Pixel spriteOffset = {0, 0});
	// Default arguments for static objects. Like trees, bathtubs or cripples.

	Entity(
		std::string uid = "",
		Pos pos = {0, 0},
		std::string sprite = std::string("assets/sprites/misc/backupSprite.png"),
		int hitboxRadius = 0,
		Pixel hitboxOffset = {0, 0},
		int spriteRows = 1,
		int spriteCols = 1,
		Pixel spriteOffset = {0, 0});

	~Entity();
};

#endif // Entity_H