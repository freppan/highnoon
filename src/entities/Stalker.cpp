#include "Stalker.hpp"

Stalker::Stalker(
	Entity &prey,
	std::string uid,
	Pos pos,
	std::string sprite,
	Pixel hitboxDimension,
	Pixel hitboxOffset,
	int spriteRows,
	int spriteCols,
	Pixel spriteOffset)
	: Entity(uid, pos, sprite, hitboxDimension, hitboxOffset, spriteRows, spriteCols, spriteOffset),
	  m_prey(prey)
{
}

Stalker::Stalker(
	Entity &prey,
	std::string uid,
	Pos pos,
	std::string sprite,
	int hitboxRadius,
	Pixel hitboxOffset,
	int spriteRows,
	int spriteCols,
	Pixel spriteOffset)
	: Entity(uid, pos, sprite, hitboxRadius, hitboxOffset, spriteRows, spriteCols, spriteOffset),
	  m_prey(prey)
{
}

void Stalker::ChildUpdate(double elapsedTime){
	Pos direction = (m_prey.GetPos() - m_pos) / (m_prey.GetPos() - m_pos).magnitude(); 
	Pos movement = direction * m_speed * float(elapsedTime);
	m_pos = m_pos + movement;

	m_direction.x = (movement.x > 0) ? 1 : -1;
	m_direction.y = (movement.y > 0) ? 1 : -1;
	if (Collisions::CheckCollision(m_prey, *this))
	{
		m_prey.Fling(direction, 0.4);
		m_prey.Damage(40.0f);
	}
}

Entity *Stalker::Clone()
{
	return new Stalker(*this);
}
