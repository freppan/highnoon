#ifndef STALKER_H
#define STALKER_H

#include "Constants.hpp"
#include "SoundManager.hpp"
#include "GraphicsManager.hpp"
#include "Entity.hpp"
#include "util/PixelPos.hpp"
#include "util/Collisions.hpp"


class Stalker : public Entity
{
private:
    Entity &m_prey;
    float m_speed = 2;

public:
    void ChildUpdate(double elapsedTime) override;
	Entity *Clone() override;
    Stalker(
        Entity &prey,
        std::string uid = "",
        Pos pos = {0, 0},
        std::string sprite = std::string("assets/sprites/misc/backupSprite.png"),
        Pixel hitboxDimension = {0, 0},
        Pixel hitboxOffset = {0, 0},
        int spriteRows = 1,
        int spriteCols = 1,
        Pixel spriteOffset = {0, 0});

    Stalker(
        Entity &prey,
        std::string uid = "",
        Pos pos = {0, 0},
        std::string sprite = std::string("assets/sprites/misc/backupSprite.png"),
        int hitboxRadius = 0,
        Pixel hitboxOffset = {0, 0},
        int spriteRows = 1,
        int spriteCols = 1,
        Pixel spriteOffset = {0, 0});

};

#endif