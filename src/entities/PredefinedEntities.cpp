#include "entities/PredefinedEntities.hpp"

PredefinedEntities::PredefinedEntities(Player &player)
{
    m_NPC.insert({"Spike", new Entity("NPC/Spike", {0, 0}, "assets/sprites/characters/friend/Spike.png", 6, {0, 0}, 4, 5)});
    m_NPC.insert({"mysterious_blob", new Stalker(player, "NPC/mysterious_blob", {0, 0}, "assets/sprites/characters/foe/mysterious_blob.png", 3, {0, 0}, 4, 4)});

    m_vegetation.insert({"Fir_Tree", new Entity("vegetation/Fir_Tree", {0, 0}, "assets/sprites/vegetation/trees/fir.png", 3, {0, 0}, 1, 1, {0, 21})});

    m_buildings.insert({"Log_Cabin", new Entity("buildings/Log_Cabin", {0, 0}, "assets/sprites/buildings/log_cabin.png", {16, 16})});

    m_furniture.insert({"Bar_Stool", new Entity("furniture/Bar_Stool", {0, 0}, "assets/sprites/furniture/bar_stool.png", {16, 16})});
    m_furniture.insert({"Tiny_Stool", new Entity("furniture/Tiny_Stool", {0, 0}, "assets/sprites/furniture/tiny_stool.png", 0)});
    m_furniture.insert({"Round_Table", new Entity("furniture/Round_Table", {0, 0}, "assets/sprites/furniture/table.png", 12)});
    m_furniture.insert({"Bar_Top0", new Entity("furniture/Bar_Top0", {0, 0}, "assets/sprites/furniture/bar_top0.png", {16, 16})});
    m_furniture.insert({"Bar_Top1", new Entity("furniture/Bar_Top1", {0, 0}, "assets/sprites/furniture/bar_top1.png", {16, 16})});
    m_furniture.insert({"Bar_Middle", new Entity("furniture/Bar_Middle", {0, 0}, "assets/sprites/furniture/bar_middle.png", {16, 20})});
    m_furniture.insert({"Bench", new Entity("furniture/Bench", {0, 0}, "assets/sprites/furniture/bench.png", {20, 4}, {0, 0}, 1, 1, {0, 4})});

    m_misc.insert({"Wall_Teleport", new Entity("misc/Wall_Teleport", {0, 0}, "assets/sprites/misc/wall_teleport.png", 0, {0, 0}, 1, 1, {0, 16})});

    m_collection.insert({"NPC", m_NPC});
    m_collection.insert({"vegetation", m_vegetation});
    m_collection.insert({"buildings", m_buildings});
    m_collection.insert({"furniture", m_furniture});
    m_collection.insert({"misc", m_misc});
}

PredefinedEntities::~PredefinedEntities()
{
}
