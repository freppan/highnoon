#ifndef PREDEFINED_ENTITIES_H
#define PREDEFINED_ENTITIES_H

#include <map>
#include <string>
#include "SoundManager.hpp"
#include "GraphicsManager.hpp"
#include "entities/Entity.hpp"
#include "entities/Player.hpp"
#include "entities/Stalker.hpp"

class PredefinedEntities
{
public:
    std::map<std::string, Entity*> m_NPC;
    std::map<std::string, Entity*> m_vegetation;
    std::map<std::string, Entity*> m_buildings;
    std::map<std::string, Entity*> m_furniture;
    std::map<std::string, Entity*> m_misc;

    std::map<std::string, std::map<std::string, Entity*>> m_collection;

    PredefinedEntities(Player &player);
    ~PredefinedEntities();
};

#endif