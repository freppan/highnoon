#include "Player.hpp"
#include <iostream>
void Player::ChildUpdate(double elapsedTime)
{
	/* Adapt speed when moving diagonally. Only works for 8 directions */
	float diagFactor = 0.707;
	float appliedSpeed = m_speed;
	if (abs(m_direction.x) && abs(m_direction.y))
	{
		appliedSpeed *= diagFactor;
	}

	m_pos.x += m_direction.x * appliedSpeed * elapsedTime;
	m_pos.y += m_direction.y * appliedSpeed * elapsedTime;

	// right boundary
	if (m_pos.x > m_xMax)
		m_pos.x = m_xMax;

	// left boundary
	if (m_pos.x < 0)
		m_pos.x = 0;

	// bottom boundary
	if (m_pos.y > m_yMax)
		m_pos.y = m_yMax;

	// upper boundary
	if (m_pos.y < 0)
		m_pos.y = 0;
}

void Player::Controller(SDL_Event keyEvent){
	switch (keyEvent.type)
	{
	case SDL_KEYDOWN:
		// keyboard API for key pressed
		switch (keyEvent.key.keysym.scancode)
		{
		case SDL_SCANCODE_W:
		case SDL_SCANCODE_UP:
			m_direction.y = -1;
			break;
		case SDL_SCANCODE_A:
		case SDL_SCANCODE_LEFT:
			m_direction.x = -1;
			break;
		case SDL_SCANCODE_S:
		case SDL_SCANCODE_DOWN:
			m_direction.y = 1;
			break;
		case SDL_SCANCODE_D:
		case SDL_SCANCODE_RIGHT:
			m_direction.x = 1;
			break;
		default:
			break;
		}
		break;

	case SDL_KEYUP:
		switch (keyEvent.key.keysym.scancode)
		{
		case SDL_SCANCODE_W:
		case SDL_SCANCODE_UP:
			if (m_direction.y < 0)
				m_direction.y = 0;
			break;
		case SDL_SCANCODE_A:
		case SDL_SCANCODE_LEFT:
			if (m_direction.x < 0)
				m_direction.x = 0;
			break;
		case SDL_SCANCODE_S:
		case SDL_SCANCODE_DOWN:
			if (m_direction.y > 0)
				m_direction.y = 0;
			break;
		case SDL_SCANCODE_D:
		case SDL_SCANCODE_RIGHT:
			if (m_direction.x > 0)
				m_direction.x = 0;
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}

}


