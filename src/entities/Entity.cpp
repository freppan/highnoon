#include "Entity.hpp"
#include <iostream>

void Entity::RenderPos(Pixel pixel)
{
	SDL_Renderer *renderer = g_gfx.GetRenderer();
	SDL_SetRenderDrawColor(renderer, 0, 255, 0, 0);

	for (int i = -5; i <= 5; i++)
	{
		// Horizontal Line
		SDL_RenderDrawPoint(renderer, pixel.x + i, pixel.y - 1);
		SDL_RenderDrawPoint(renderer, pixel.x + i, pixel.y);
		SDL_RenderDrawPoint(renderer, pixel.x + i, pixel.y + 1);

		// Vertical Line
		SDL_RenderDrawPoint(renderer, pixel.x - 1, pixel.y + i);
		SDL_RenderDrawPoint(renderer, pixel.x, pixel.y + i);
		SDL_RenderDrawPoint(renderer, pixel.x + 1, pixel.y + i);
	}

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0); // Always restore DrawColor for a black background
}

Hitbox *Entity::GetHitbox()
{
	return m_hitbox;
}

void Entity::RenderHitbox(Pixel pixel)
{
	RenderPos(pixel);
	pixel = m_hitbox->ApplyOffset(pixel);

	if (m_hitbox->GetDescription() == "CircularHitbox")
	{
		RenderCircularHitbox(pixel);
	}
	else if (m_hitbox->GetDescription() == "RectangularHitbox")
	{
		RenderRectangularHitbox(pixel);
	}
}

void Entity::RenderCircularHitbox(Pixel pixel)
{
	float radius = m_hitbox->GetRadius() * TILESIZE * SCALE;

	SDL_Renderer *renderer = g_gfx.GetRenderer();
	SDL_SetRenderDrawColor(renderer, 0, 255, 0, 0);

	for (float angle = 0.0f; angle < 2 * 3.14; angle += 2 * 3.14 / 80)
	{
		SDL_RenderDrawPoint(renderer, pixel.x + radius * cosf(angle), pixel.y + radius * sinf(angle));
	}

	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0); // Always restore DrawColor for a black background
}

void Entity::RenderRectangularHitbox(Pixel pixel)
{
	Pos dimension = m_hitbox->GetDimensions();
	dimension.x *= TILESIZE * SCALE;
	dimension.y *= TILESIZE * SCALE;

	SDL_Renderer *renderer = g_gfx.GetRenderer();
	SDL_SetRenderDrawColor(renderer, 0, 255, 0, 0);
	for (int i = pixel.x - dimension.x / 2; i < pixel.x + dimension.x / 2; i++)
	{
		SDL_RenderDrawPoint(renderer, i, pixel.y - dimension.y / 2);
		SDL_RenderDrawPoint(renderer, i, pixel.y + dimension.y / 2);
	}

	for (int i = pixel.y - dimension.y / 2; i < pixel.y + dimension.y / 2; i++)
	{
		SDL_RenderDrawPoint(renderer, pixel.x - dimension.x / 2, i);
		SDL_RenderDrawPoint(renderer, pixel.x + dimension.x / 2, i);
	}
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0); // Always restore DrawColor for a black background
}

void Entity::RenderGraphics(Pixel pixel, double elapsedTime)
{
	// Adapt char render pos to cam pos
	if (m_direction.x != 0 || m_direction.y != 0)
	{
		if (m_direction.x)
		{
			if (m_direction.x > 0)
			{
				m_animationFrame.y = m_animationFrame.h * DIR_RIGHT;
			}
			else
			{
				m_animationFrame.y = m_animationFrame.h * DIR_LEFT;
			}
		}
		else
		{
			if (m_direction.y > 0)
			{
				m_animationFrame.y = m_animationFrame.h * DIR_DOWN;
			}
			else
			{
				m_animationFrame.y = m_animationFrame.h * DIR_UP;
			}
		}
		m_countTime += elapsedTime;
		if (m_animationFrame.x == 0)
		{
			m_animationFrame.x = m_animationFrame.w;
		}

		if (m_countTime > 0.15)
		{
			m_countTime = 0;

			m_animationFrame.x += m_animationFrame.w;
			if (m_animationFrame.x >= m_texture->width())
				m_animationFrame.x = m_animationFrame.w;
		}
	}
	else
	{
		m_animationFrame.x = 0;
	}

	pixel.x += m_spriteOffset.x * SCALE;
	pixel.y += m_spriteOffset.y * SCALE;

	if (m_colorMod != Color{255, 255, 255})
	{
		m_texture->ColorMod(m_colorMod.r, m_colorMod.g, m_colorMod.b);
	}

	if (m_texture->staticSprite())
	{
		g_gfx.Render(*m_texture, pixel);
	}
	else
	{
		g_gfx.Render(*m_texture, m_animationFrame, pixel);
	}

	if (m_colorMod != Color{255, 255, 255})
	{ // Reset colorMod. Other Entities can share the same texture.
		m_texture->ColorMod(255, 255, 255);
	}
}

void Entity::Update(double elapsedTime)
{
	HandleFling(elapsedTime);

	m_resetColorModTimer = std::max(0.0f, m_resetColorModTimer - float(elapsedTime));
	if (m_resetColorModTimer == 0.0f)
	{
		SetColorMod(255, 255, 255);
	}

	ChildUpdate(elapsedTime);
}

void Entity::ChildUpdate(double elapsedTime) {}

void Entity::SetSpriteDirection(Entity::Direction direction)
{
	m_animationFrame.y = m_animationFrame.h * direction;
}

void Entity::SetPos(Pos pos)
{
	m_pos = pos;
}

Pos Entity::GetPos()
{
	return m_pos;
}

void Entity::Damage(float damage, Color color)
// Default color is lagom red.
// Use for example blue for ice dmg.
{
	// m_health = std::max(0.0f, m_health - damage);
	SetColorMod(color.r, color.g, color.b);
	m_resetColorModTimer += damage / 60.0f;
}

void Entity::Fling(Pos direction, float speed)
{
	if (m_flingSpeed == 0.0f)
	{
		m_flingDirection = direction;
		m_flingSpeed = speed;
	}
}

void Entity::HandleFling(double elapsedTime)
{
	if (m_flingSpeed == 0.0f)
	{
		return;
	}
	SetPos(m_pos + m_flingDirection * m_flingSpeed * 50 * elapsedTime);
	m_flingSpeed = std::max(0.0f, m_flingSpeed - float(elapsedTime));
}

std::string Entity::GetUid()
{
	return m_uid;
}

void Entity::SetColorMod(Uint8 r, Uint8 g, Uint8 b)
{
	m_colorMod = Color{r, g, b};
}

void Entity::SetMapBorders(int x, int y)
{
	m_xMax = x;
	m_yMax = y;
}

Entity *Entity::Clone()
{
	return new Entity(*this);
}

Entity::Entity(
	std::string uid,
	Pos pos,
	std::string sprite,
	int hitboxRadius,
	Pixel hitboxOffset,
	int spriteRows,
	int spriteCols,
	Pixel spriteOffset)
	: m_uid(uid),
	  m_texture(g_gfx.RequestTexture(sprite, spriteRows, spriteCols)),
	  m_spriteOffset(spriteOffset)
{
	m_pos.x = pos.x;
	m_pos.y = pos.y + float(rand() % 100) / 10000.0f; // Rendering order is dependant on y coord. Set a negligable error for nearby entities to have different y vals, and fixed rendering order.
	m_animationFrame.w = m_texture->width() / m_texture->spriteCols();
	m_animationFrame.h = m_texture->height() / m_texture->spriteRows();
	m_animationFrame.x = 0;
	m_animationFrame.y = 0;

	if (hitboxRadius == 0)
	{
		m_noHitbox = NoHitbox();
		m_hitbox = &m_noHitbox;
	}
	else
	{
		m_circularHitbox = CircularHitbox(hitboxRadius, hitboxOffset);
		m_hitbox = &m_circularHitbox;
	}
}

Entity::Entity(
	std::string uid,
	Pos pos,
	std::string sprite,
	Pixel hitboxDimension,
	Pixel hitboxOffset,
	int spriteRows,
	int spriteCols,
	Pixel spriteOffset)
	: m_uid(uid),
	  m_texture(g_gfx.RequestTexture(sprite, spriteRows, spriteCols)),
	  m_spriteOffset(spriteOffset)
{
	m_pos.x = pos.x;
	m_pos.y = pos.y + float(rand() % 100) / 10000.0f; // Rendering order is dependant on y coord. Set a negligable error for nearby entities to have different y vals, and fixed rendering order.

	m_animationFrame.w = m_texture->width() / m_texture->spriteCols();
	m_animationFrame.h = m_texture->height() / m_texture->spriteRows();
	m_animationFrame.x = 0;
	m_animationFrame.y = 0;

	if (hitboxDimension.x == 0 && hitboxDimension.y == 0)
	{
		m_noHitbox = NoHitbox();
		m_hitbox = &m_noHitbox;
	}
	else
	{
		m_rectangularHitbox = RectangularHitbox(hitboxDimension, hitboxOffset);
		m_hitbox = &m_rectangularHitbox;
	}
}

Entity::~Entity(){};