#ifndef PLAYER_H
#define PLAYER_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>
#include "Constants.hpp"
#include "SoundManager.hpp"
#include "GraphicsManager.hpp"
#include "Entity.hpp"

class Player : public Entity
{

public:
    void ChildUpdate(double elapsedTime) override;
    void Controller(SDL_Event keyEvent);
    using Entity::Entity;
};

#endif