#ifndef SCENE_H
#define SCENE_H

#include <string>
#include "SoundManager.hpp"
#include "GraphicsManager.hpp"

class SceneManager;
class Scene
{
public:
	SceneManager &m_sceneManager;
	virtual bool Update(double elapsedTime) = 0;
	virtual void RenderGraphics(double elapsedTime) = 0;
	virtual void Load() = 0;   // Load sprites and audio needed for the scene
	virtual void Unload() = 0; // Unload sprites and audio to avoid memory leaks
	bool Cycle(double elapsedTime);
	Scene(SceneManager &sceneManager);
};
#endif // SCENE_H