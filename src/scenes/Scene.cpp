#include "Scene.hpp"

bool Scene::Cycle(double elapsedTime) {
	bool keep_playing = Update(elapsedTime);
	RenderGraphics(elapsedTime);
	return keep_playing;
}

Scene::Scene(SceneManager& sceneManager)
	: m_sceneManager(sceneManager)
{}