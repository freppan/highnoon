#include "StardewBase.hpp"
#include <iostream>

StardewBase::Tile::Tile(
	std::string sprite,
	int stateOfMatter)
	: m_texture(g_gfx.RequestTexture(sprite, 1, 1)),
	  m_stateOfMatter(stateOfMatter),
	  m_sprite(sprite)
{
	if (stateOfMatter == 1)
	{
		m_hitbox = &m_rectangularHitbox;
	}
	else
	{
		m_hitbox = &m_noHitbox;
	}
}

Pixel StardewBase::PosToPixel(Pos pos)
{
	Pixel pixel;

	if (m_mapWidth > CAMWIDTH)
	{
		pixel.x = int((pos.x - m_camera.x) * SCALE * TILESIZE + SCREENWIDTH / 2);
	}
	else
	{
		pixel.x = int((pos.x - m_mapWidth / 2) * SCALE * TILESIZE + SCREENWIDTH / 2);
	}

	if (m_mapHeight > CAMHEIGHT)
	{
		pixel.y = int((pos.y - m_camera.y) * SCALE * TILESIZE + SCREENHEIGHT / 2);
	}
	else
	{
		pixel.y = int((pos.y - m_mapHeight / 2) * SCALE * TILESIZE + SCREENHEIGHT / 2);
	}
	return pixel;
}

Pos StardewBase::PixelToPos(Pixel pixel)
{
	Pos pos;
	if (m_mapWidth > CAMWIDTH)
	{
		pos.x = float(pixel.x - SCREENWIDTH / 2) / (SCALE * TILESIZE) + m_camera.x;
	}
	else
	{
		pos.x = float(pixel.x - SCREENWIDTH / 2) / (SCALE * TILESIZE) + m_mapWidth / 2;
	}

	if (m_mapHeight > CAMHEIGHT)
	{
		pos.y = float(pixel.y - SCREENHEIGHT / 2) / (SCALE * TILESIZE) + m_camera.y;
	}
	else
	{
		pos.y = float(pixel.y - SCREENHEIGHT / 2) / (SCALE * TILESIZE) + m_mapHeight / 2;
	}
	return pos;
}

void StardewBase::HandleKeyEvents()
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{

		/*	Uncomment this block to print mouse position on map. Good for debugging */
		/*
		if (event.type == SDL_MOUSEMOTION)
		{
			Pixel pixel;
			SDL_GetMouseState(&pixel.x, &pixel.y);
			Pos mousePos = PixelToPos(pixel);
			std::cout << "mousePos: { x: " << mousePos.x << ", y: " << mousePos.y << " }\n";
		}
		*/

		if (event.type == SDL_KEYDOWN && event.key.keysym.scancode == SDL_SCANCODE_ESCAPE)
		{
			m_sceneManager.QueueSceneChange("MENU");
		}
		m_player->Controller(event);
	}
}

bool StardewBase::Update(double elapsedTime)
{

	// PrintFPS(elapsedTime);

	// Intercept key events that needs to be handled by the scene.
	HandleKeyEvents();

	m_player->Update(elapsedTime);

	if(!m_pauseEntities)
	{
		for(auto &entity : m_entities)
		{
			entity->Update(elapsedTime);
		}
	}

	Pos camera = m_player->GetPos();
	std::tie(m_camera.x, m_camera.y) = {camera.x, camera.y};

	// right boundary
	if (m_camera.x > m_mapWidth - CAMWIDTH / 2)
		m_camera.x = m_mapWidth - CAMWIDTH / 2;

	// left boundary
	if (m_camera.x < CAMWIDTH / 2);
		m_camera.x = CAMWIDTH / 2;

	// bottom boundary
	if (m_camera.y > m_mapHeight - CAMHEIGHT / 2)
		m_camera.y = m_mapHeight - CAMHEIGHT / 2;

	// upper boundary
	if (m_camera.y < CAMHEIGHT / 2)
		m_camera.y = CAMHEIGHT / 2;

	for(auto &colEntity : m_entitiesToCollisionCheck)
	{
		for (auto &entity : m_entities)
		{
			if(colEntity->GetPos() != entity->GetPos())
			{
				Collisions::CheckCollision(*colEntity, *entity);
			}
		}
	}

	for (int i = -1; i < 3; i++)
	{
		for (int j = -1; j < 3; j++)
		{
			Pos tilePos = m_player->GetPos().floor();
			tilePos.x += float(i);
			tilePos.y += float(j);

			if (tilePos.x >= 0 && tilePos.x < m_mapWidth && tilePos.y >= 0 && tilePos.y < m_mapHeight)
			{
				Entity tileEntity = Entity(
					"",
					tilePos,
					"assets/sprites/misc/empty_sprite.png",
					m_map[int(tilePos.y)][int(tilePos.x)]->GetHitbox()->GetPixelDimensions(),
					{TILESIZE / 2, TILESIZE / 2});

				for(auto &colEntity : m_entitiesToCollisionCheck)
				{
					Collisions::CheckCollision(*colEntity, tileEntity);
				}
			}
		}
	}

	UpdateScene(elapsedTime);
	return true;
}

void StardewBase::RenderGraphics(double elapsedTime)
{
	g_gfx.ResetRenderArea();

	// Calculate where the cameras edges are. Used for render distance
	int marginOutsideScreen = 5; // Like utfall
	m_camera.xMin = std::max(0, int(m_camera.x - CAMWIDTH / 2) - marginOutsideScreen);
	m_camera.xMax = std::min(int(m_camera.x + CAMWIDTH / 2) + marginOutsideScreen, m_mapWidth);
	m_camera.yMin = std::max(0, int(m_camera.y - CAMHEIGHT / 2) - marginOutsideScreen);
	m_camera.yMax = std::min(int(m_camera.y + CAMHEIGHT / 2) + marginOutsideScreen, m_mapHeight);

	for (int i = m_camera.xMin; i < m_camera.xMax; i++)
	{
		for (int j = m_camera.yMin; j < m_camera.yMax; j++)
		{
			m_map[j][i]->Render(PosToPixel({float(i), float(j)}));
		}
	}

	/* Process entities and items */
	std::vector<Entity *> entitiesToRender;
	entitiesToRender.push_back(m_player);

	for (auto &entity : m_entities)
	{
		if (entity->GetPos().x >= m_camera.xMin && entity->GetPos().x <= m_camera.xMax && entity->GetPos().y >= m_camera.yMin && entity->GetPos().y <= m_camera.yMax)
		{
			entitiesToRender.push_back(entity);
		}
	}

	auto sortRule = [](Entity *const c0, Entity *const c1) -> bool
	{
		return c0->GetPos().y < c1->GetPos().y;
	};
	std::sort(entitiesToRender.begin(), entitiesToRender.end(), sortRule);

	/* Render entities */
	for (int i = 0; i < entitiesToRender.size(); i++)
	{
		Pixel pixel = PosToPixel(entitiesToRender[i]->GetPos());
		entitiesToRender[i]->RenderGraphics(pixel, elapsedTime);
	}

	if (m_renderHitboxes)
	{
		for (int i = 0; i < entitiesToRender.size(); i++)
		{
			Pixel pixel = PosToPixel(entitiesToRender[i]->GetPos());
			entitiesToRender[i]->RenderHitbox(pixel); // For hitbox debug. FPS KILLER!
		}

		for (int i = 0; i < m_mapWidth; i++)
		{
			for (int j = 0; j < m_mapHeight; j++)
			{
				Entity tileEntity = Entity("", {float(i), float(j)}, "assets/sprites/misc/empty_sprite.png", m_map[j][i]->GetHitbox()->GetPixelDimensions(), {8, 8});
				tileEntity.RenderHitbox(PosToPixel({float(i), float(j)}));
			}
		}
	}

	RenderScene(elapsedTime);
	g_gfx.RenderBuffer();
}

void StardewBase::ReadTilesFromFile(std::string fileName)
{
	std::ifstream fileHandler;
	fileHandler.seekg(std::ios::beg);
	fileHandler.open(fileName, std::fstream::in);
	std::string line;
	std::vector<Tile *> tilesRow;
	PredefinedEntities predefines(*m_player);

	enum
	{
		TILES,
		MAP,
		ENTITIES
	} fileSection;

	if (fileHandler.is_open())
	{
		while (getline(fileHandler, line))
		{
			// Check for delimiter at middle of file.
			if (line.find('#') != std::string::npos)
			{
				if (line == "# TILES #")
				{
					fileSection = TILES;
				}
				else if (line == "# MAP #")
				{
					fileSection = MAP;
				}
				else if (line == "# ENTITIES #")
				{
					fileSection = ENTITIES;
				}
				else
				{
					std::cout << "MAP FILE: BROKEN HEADER\n";
				}
			}

			// Parse first half of file.
			else if (fileSection == TILES)
			{
				size_t pos = 0;
				std::string path;
				std::string delimiter = ";";
				int state;
				if ((pos = line.find(delimiter)) != std::string::npos) // Devide string into "Path" and "State" at ";".
				{
					path = line.substr(0, pos);
					state = std::stoi(line.substr(pos + delimiter.length()));
					m_tiles.push_back(new Tile(path, state));
				}
			}
			// Parse last half of file.
			else if (fileSection == MAP)
			{
				std::stringstream ss(line);
				for (int i; ss >> i;)
				{
					tilesRow.push_back(m_tiles[i]);
					if (ss.peek() == ',')
						ss.ignore();
				}
				m_map.push_back(tilesRow);
				tilesRow.clear();
			}
			else if (fileSection == ENTITIES)
			{
				std::string delimiter = ";";
				std::size_t posX = line.find(delimiter);
				std::size_t posY = line.find(delimiter, posX + 1);

				Pos pos = {
					std::stof(line.substr(posX + 1, posY - 1 - posX)),
					std::stof(line.substr(posY + 1))};

				std::string uid = line.substr(0, posX);

				delimiter = "/";
				std::size_t uid_divider = uid.find(delimiter);
				std::string section = uid.substr(0, uid_divider);
				std::string name = uid.substr(uid_divider + 1);

				m_entities.push_back(new Entity(*predefines.m_collection.at(section).at(name)));
				m_entities.back()->SetPos(pos);
			}
		}
	}
	fileHandler.close();

	m_mapWidth = m_map[0].size();
	m_mapHeight = m_map.size();
	m_player->SetMapBorders(m_mapWidth, m_mapHeight);
}

void StardewBase::PrintFPS(double elapsedTime)
{
	m_frameCounter++;
	m_fpsTimer -= elapsedTime;
	if (m_fpsTimer < 0.0)
	{
		std::cout << m_frameCounter << " fps\n";
		m_frameCounter = 0;
		m_fpsTimer = 1.0;
	}
}

void StardewBase::Load()
{
	LoadScene();
}

void StardewBase::Unload()
{

	while(!m_tiles.empty()) delete m_tiles.back(), m_tiles.pop_back();
	while(!m_entities.empty()) delete m_entities.back(), m_entities.pop_back();
	m_map.clear();

	UnloadScene();
}

StardewBase::StardewBase(SceneManager &sceneManager, Player *player)
	: Scene(sceneManager)
{
	m_player = player;
}