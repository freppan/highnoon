#ifndef CONSTANTS_H
#define CONSTANTS_H

/* Core */
inline constexpr unsigned short SCREENWIDTH{ 1920 };
inline constexpr unsigned short SCREENHEIGHT{ 1080 };

/*  Constants for Stardew Style Scenes  */
inline constexpr unsigned short TILESIZE{ 16 };
inline constexpr unsigned short CAMWIDTH{ 30 }; //Number or tiles to fit in camera
inline constexpr float CAMHEIGHT{ CAMWIDTH * (float(SCREENHEIGHT) / float(SCREENWIDTH)) };
inline constexpr unsigned short SCALE{ int(SCREENWIDTH / TILESIZE / CAMWIDTH) };

#endif //CONSTANTS_H